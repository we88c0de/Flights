# FS1

Mini tutorials: [Git](https://gitlab.com/korsa719/Flights/blob/master/docs/tutorials/GIT.md), 
[Maven](https://gitlab.com/korsa719/Flights/blob/master/docs/tutorials/MAVEN.md),
[Linux Aliases](https://gitlab.com/korsa719/Flights/blob/master/docs/tutorials/LINUX_ALIASES.md).

## Instagram

### Tasks

1. Create new project.
2. Create DB schema.
3. Add User as a entity.
4. Insert User into database.
5. Select User with in JDBC.
6. Integrate Hibernate into project.
7. Utilize Transactions for DB operations.
8. Integrate Spring Data.
9. Integrate String Boot, IoC into project.
10. Integrate Spring Security.
11. Integrate Spring MVC.
12. Write down server endpoint that serve users from DB as Json.
13. Login page.
14. Add User registration.

## Flights

#### Goal
Create rest application that shows airport's shedule flights.

#### Tools
* [Apache Maven](https://maven.apache.org/) is a software project management and comprehension tool.
* [Eclipse Jetty](https://www.eclipse.org/jetty) provides a Web server and javax.servlet container, plus support for HTTP/2, WebSocket, OSGi, JMX, JNDI, JAAS and many other integrations.
* [JUnit ](http://junit.org/junit4) is a simple framework to write repeatable tests. It is an instance of the xUnit architecture for unit testing frameworks. 

#### Installiation and Execution
To start rest application open your *Terminal* and type `bash start.sh` in it.

``$ cd {working directory}``   
``$ bash start.sh``

Open your browser and type `localhost:8080` in address field, if you see " Welcome to British airport " that means your application is working.

#### Rules for contributors
The big request to participants not to write in the file README.md any crap and dregs, add only useful text.

#### REST framework

Framework that implements [REST](https://en.wikipedia.org/wiki/Representational_state_transfer) standard in java.
Standart called [JAX-RS](https://jcp.org/en/jsr/detail?id=339) and reference implementation called [Jersey](https://jersey.github.io/).
